module Programa
{
    struct Ventas
    {
        string arrayVentas;
    }
    struct Stock
    {
        string arrayStock;
    }
    struct Venta
    {
        float sellerId;
        string arrayVenta;
        double valor;
    }
    interface Almacen
    {
	void printString(string s);
        void getStockData(Stock data);
        void getVentasRegister(Ventas data);
        void makeVenta(Venta data);
        void updateStockData(string arrayData);
        void startVentas();
        void closeVentas();
        void startManager();
        void close();
    }
}

