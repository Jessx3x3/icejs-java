import Programa.*;

public class AlmacenI implements Almacen
{
    public void printString(String s, com.zeroc.Ice.Current current)
    {
        System.out.println(s);
    }
    public void getStockData(Stock data, com.zeroc.Ice.Current current)
    {
        System.out.println(" method: getStockData ");
    }
    public void getVentasRegister(Ventas data, com.zeroc.Ice.Current current)
    {
        System.out.println(" method: getVentasRegister ");
    }
    public void makeVenta(Venta data, com.zeroc.Ice.Current current)
    {
        System.out.println(" method: makeVenta ");
    }
    public void updateStockData(String arrayData, com.zeroc.Ice.Current current)
    {
        System.out.println(" method: updateStockData");
    }
    public void startVentas(com.zeroc.Ice.Current current)
    {
        System.out.println(" method: startVentas");
    }
    public void closeVentas(com.zeroc.Ice.Current current)
    {
        System.out.println(" method: closeVentas");
    }
    public void startManager(com.zeroc.Ice.Current current)
    {
        System.out.println(" method: startManager");
    }
    public void close(com.zeroc.Ice.Current current)
    {
        System.out.println(" method: close");
    }

}
